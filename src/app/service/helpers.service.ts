import { Injectable } from '@angular/core';
import {Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HelpersService {
  darkNav = new Subject<any>();
  constructor() { }


  darkNavStatus(data) {
    this.darkNav.next(data);
  }
}
