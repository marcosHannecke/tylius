import {Component, OnDestroy, OnInit} from '@angular/core';
import {HelpersService} from '../../../service/helpers.service';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit, OnDestroy {
  inverted: any;
  open = false;
  darkNav: boolean;
  unsubscribe: any;

  constructor(private helpers: HelpersService) {
  }

  ngOnInit() {
    // background
    window.onscroll = () => {
      this.inverted = this.open ? this.open : window.scrollY !== 0;
    };
    // Dark nav
    this.unsubscribe = this.helpers.darkNav
      .subscribe((data) => {
        this.darkNav = data;
      });
  }

  ngOnDestroy(): void {
    if (this.unsubscribe) {
      this.unsubscribe.unsubscribe();
    }
  }

  /**
   * Open menu
   */
  openMenu() {
    this.open = !this.open;
    this.inverted = this.open ? this.open : window.scrollY !== 0;
  }
}
