import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HomeComponent} from './home.component';
import {CarouselComponent} from './carousel/carousel.component';
import {AppMaterialModule} from '../../app-material.module';
import {FlexModule} from '@angular/flex-layout';

@NgModule({
  declarations: [
    HomeComponent,
    CarouselComponent,
  ],
  imports: [
    CommonModule,
    AppMaterialModule,
    FlexModule
  ]
})
export class HomeModule {
}
