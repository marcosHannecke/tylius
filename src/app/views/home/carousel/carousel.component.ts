import {Component, OnDestroy, OnInit} from '@angular/core';
import {startWith, switchMap} from 'rxjs/operators';
import {Subject, Subscription, timer} from 'rxjs';
import {HelpersService} from '../../../service/helpers.service';

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.scss']
})
export class CarouselComponent implements OnInit, OnDestroy {
  hero = 1;
  private unsubscribe: Subscription;
  source = new Subject();
  countdown = true;

  constructor(private helpers: HelpersService) { }

  ngOnInit() {
    // Interval for carousel
    this.unsubscribe = this.source
      .pipe(
        startWith(void 0),
        switchMap(() => timer(5000, 5000)),
      )
      .subscribe(() => this.nextHero());
  }

  ngOnDestroy() {
    if (this.unsubscribe) {
      this.unsubscribe.unsubscribe();
    }
  }

  /**
   * Next carousel slide
   */
  nextHero() {
    this.source.next(void 0);
    this.hero += 1;
    if (this.hero === 5) { // number of hero + 1
      this.hero = 1;
    }
    this.helpers.darkNavStatus(this.hero === 4);
  }

}
